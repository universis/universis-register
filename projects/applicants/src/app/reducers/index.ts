import {
  Action,
  ActionReducer,
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { merge } from 'lodash';
export interface AppState {
  // add your state properties here
}

export const reducers: ActionReducerMap<AppState> = {
};

function sessionStorageMergeMetaReducer<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
  let onInit = true;
  return function(state: S, action: A): S {
    const nextState = reducer(state, action);
    // init the application state.
    const savedState = JSON.parse(sessionStorage.getItem('applicants.state')) || {};
    if (onInit) {
      onInit  = false;
      return merge(nextState, savedState);
    }
    const setState = merge(savedState, nextState);
    sessionStorage.setItem('applicants.state', JSON.stringify(setState));
    return nextState;
  };
}

function localStorageMergeMetaReducer<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
  let onInit = true;
  return function(state: S, action: A): S {
    const nextState = reducer(state, action);
    // init the application state.
    const savedState = JSON.parse(localStorage.getItem('applicants.state')) || {};
    if (onInit) {
      onInit  = false;
      return merge(nextState, savedState);
    }
    const setState = merge(savedState, nextState);
    localStorage.setItem('applicants.state', JSON.stringify(setState));
    return nextState;
  };
}

const metaReducers: MetaReducer<AppState>[] = [
  localStorageMergeMetaReducer
];

export {
  sessionStorageMergeMetaReducer,
  localStorageMergeMetaReducer,
  metaReducers
};
