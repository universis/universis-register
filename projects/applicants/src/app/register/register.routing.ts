import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvancedListComponent } from '@universis/ngx-tables';
import { AdvancedFormModalData, AdvancedFormModalComponent, AdvancedFormItemResolver } from '@universis/forms';
import { ListComponent } from './components/list/list.component';
import { EditComponent } from './components/edit/edit.component';
import { MessagesComponent } from './components/messages/messages.component';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import { EnrollmentEventsComponent } from './components/enrollment-events/enrollment-events.component';
import { EnrollmentEventRootComponent } from './components/enrollment-events/events-root/event-root.component';
import { EnrollmentEventsDashboardComponent } from './components/enrollment-events/event-dashboard/enrollment-events-dashboard.component';
import { EnrollmentEventOverviewComponent } from './components/enrollment-events/event-dashboard/event-overview/event-overview.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'enrollment-events',
    pathMatch: 'full'
  },
  {
    path: 'item/:id/edit',
    component: EditComponent,
    data: <AdvancedFormModalData>{
      model: 'StudyProgramRegisterActions',
      serviceQueryParams: EditComponent.ServiceQueryParams
    },
    resolve: {
      model: AdvancedFormItemResolver
    }
  },
  {
    path: 'messages',
    component: MessagesComponent,
    data: {
      model: 'StudyProgramRegisterActionMessages',
      description: 'Register.Messages'
    }
  },
  {
    path: 'enrollment-events',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: EnrollmentEventsComponent,
        data: {
          model: 'StudyProgramEnrollmentEvents',
        }
      },
      {
        path: ':id',
        component: EnrollmentEventRootComponent,
        data: {
          title: 'Candidates Home'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list'
          },
          {
            path: 'list',
            component: ListComponent,
            data: {
              description: 'Settings.Lists.StudyProgramRegisterActions.Description',
              title: 'Register.ApplicationListTitle'
            },
            children: [
              {
                path: 'item/:id/edit',
                component: ModalEditComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'StudyProgramRegisterActions',
                  action: 'modal-edit',
                  serviceQueryParams: EditComponent.ServiceQueryParams,
                  modalOptions: {
                    modalClass: 'mx-w-90 verybigmodal'
                  },
                  closeOnSubmit: true,
                  continueLink: '.'
                },
                resolve: {
                  model: AdvancedFormItemResolver
                }
              },
              {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  action: 'new',
                  closeOnSubmit: true
                }
              },
              {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  action: 'edit',
                  closeOnSubmit: true
                },
                resolve: {
                  data: AdvancedFormItemResolver
                }
              }
            ]
          }

        ]
      }
    ]
  },
  {
    path: 'enrollment-events/:id/list',
    component: ListComponent,
    data: {
      model: 'StudyProgramRegisterActions',
      description: 'Settings.Lists.StudyProgramRegisterActions.Description'
    },
    children: [
      {
        path: 'item/:id/edit',
        component: ModalEditComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'StudyProgramRegisterActions',
          action: 'modal-edit',
          serviceQueryParams: EditComponent.ServiceQueryParams,
          modalOptions: {
            modalClass: 'mx-w-90 verybigmodal'
          },
          closeOnSubmit: true,
          continueLink: '.'
        },
        resolve: {
          model: AdvancedFormItemResolver
        }
      },
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class RegisterRoutingModule {
}
