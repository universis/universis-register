import {NgModule, CUSTOM_ELEMENTS_SCHEMA, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablesModule } from '@universis/ngx-tables';
import { ListComponent } from './components/list/list.component';
import { SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { RegisterRoutingModule } from './register.routing';
import { FormsModule } from '@angular/forms';
import { AdvancedFormsModule } from '@universis/forms';
import { RouterModule } from '@angular/router';
import { MostModule } from '@themost/angular';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { MessagesComponent } from './components/messages/messages.component';
import { SendMessageActionComponent } from './components/send-message-action/send-message-action.component';
import { RouterModalModule } from '@universis/common/routing';
import { RegisterSharedModule} from './register.shared';
import { EnrollmentEventsComponent } from './components/enrollment-events/enrollment-events.component';
import { EnrollmentEventOverviewComponent } from './components/enrollment-events/event-dashboard/event-overview/event-overview.component';
import { EnrollmentEventRootComponent } from './components/enrollment-events/events-root/event-root.component';
import { EnrollmentEventsDashboardComponent } from './components/enrollment-events/event-dashboard/enrollment-events-dashboard.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    MostModule,
    RouterModule,
    TranslateModule,
    TablesModule,
    FormsModule,
    AdvancedFormsModule,
    RouterModalModule,
    RegisterRoutingModule,
    NgxExtendedPdfViewerModule,
    NgxDropzoneModule,
    RegisterSharedModule
  ],
  declarations: [
    ListComponent,
    EnrollmentEventsComponent,
    EnrollmentEventRootComponent,
    EnrollmentEventsDashboardComponent,
    EnrollmentEventOverviewComponent,
    MessagesComponent
  ],
  exports: [
  ],
  entryComponents: [
    SendMessageActionComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RegisterModule implements OnInit {
  constructor(private _translateService: TranslateService) {

  }

  async ngOnInit() {
    //
  }
}
