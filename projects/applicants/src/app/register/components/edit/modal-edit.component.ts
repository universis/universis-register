import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register-modal-edit',
  templateUrl: './modal-edit.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ModalEditComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  dataSubscription: Subscription;
  public showActions = false;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _translateService: TranslateService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'mx-w-90 verybigmodal';
    // this.modalTitle = this._translateService.instant('Settings.EditItem') + ' ('
    //   + this._translateService.instant('Settings.Lists.StudyProgramRegisterActions.Title') + ')';
    this.dataSubscription = this.activatedRoute.data.subscribe((data) => {
      if (data.action === 'modal-edit') {
        this.showActions = true;
      }
    });
  }

  ngOnInit(): void {
    this.cancelButtonClass = 'd-none';
    this.okButtonText = this._translateService.instant('Settings.Close');
  }

  cancel(): Promise<any> {
    return super.close();
  }

  ok(): Promise<any> {
    return super.close();
  }

}

