import { Component, Input, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-register-enrollment-events-enrollment-events-dashboard',
  templateUrl: './enrollment-events-dashboard.component.html'
})
export class EnrollmentEventsDashboardComponent implements OnInit {

  public tabs: any[];
  public enrollmentEvent: any;
  @Input() enrollmentEventId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit(): Promise<void> {
    this.enrollmentEventId = this._activatedRoute.snapshot.params.id;

    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

  }

}
